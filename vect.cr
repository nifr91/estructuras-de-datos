#!/usr/bin/env crystal 

# Vector, es una plantilla que permite disponer de arreglos (contenedor 
# secuencial) que puede cambiar de tamaño de manera dinámica 
#
class Vect(T) 

  # Constructor básico, crear un arreglo de tamaño n 
  def initialize(n) 
    @buff = Pointer(T).malloc(n)
    @size = 0 
    @buff_size = n
  end 

  # devuelve el número de elementos 
  def size() @size end 

  # Insertar al final 
  def push(val) 
    check_size 
    @buff[@size] = val
    @size += 1 
  end 

  # Quitar último elemento 
  def pop?() 
    return nil if @size == 0 
    @size -= 1 
    @size  = (@size < 0) ? 0 : @size
  end
 
  # Acceder a los elementos 
  def [](index) @buff[check_bounds index] end
  
  # Asignar los elementos 
  def []=(index,val) @buff[check_bounds index] = val end 

  # Eliminar todos los elementos 
  def clear() @size = 0 end 

  # Reservar para al menos n objetos 
  def reserve(n)
    if n > @buff_size 
      @buff = @buff.realloc(n)
      @buff_size = n
    end 
  end 

  # Liberar memoria no usada
  def shrink
    @buff = @buff.realloc(@size) 
    @buff_size = @size
  end 

  private def check_bounds(index) 
    index += @size if index < 0
    raise "index out of bounds" unless (0 <= index) && (index < @size)
    index
  end 
  
  private def check_size 
    if @size ==  @buff_size 
      @buff_size *= 2 
      reserve(@buff_size) 
    end 
  end 
 
  def inspect(io) to_s(io) end 
  def to_s(io) 
    (0 ... @size-1).each do |k|
      io << @buff[k] << ','
    end 
    io << @buff[@size-1]
    io 
  end 
end 


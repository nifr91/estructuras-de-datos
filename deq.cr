#!/usr/bin/env crystal 


# Deque is a generalized version of queue data structure that 
# allows insert and delete at both ends.
#
class Deq(T) 

  @front : Int32
  @back  : Int32
  @buff  : Pointer(T) 
  @buff_size  : Int32 

  def initialize
    @front = -1 
    @back  = -1
    @buff_size = 2
    @buff = Pointer(T).malloc(@buff_size) 
  end 


  # Obtener el primer elemento 
  def front?
    return nil if self.empty? 
    @buff[@front]
  end 

  # Obtener el último elemento 
  def back?
    return nil if self.empty?
    @buff[@back] 
  end 

  # Regresa true si la lista esta vacia 
  def empty?() @front == -1 end 

  # Agrega un elemento al final
  def push(val : T) 
    check_resize
    if @front == -1 
      @front = 0
      @back = 0 
    else 
      @back = (@back + 1) % @buff_size 
    end 
    @buff[@back] = val 
    self 
  end 

  # Elimina el último elemento 
  def pop?() 
    return nil if self.empty? 
    if @front == @back 
      val = @buff[@front] 
      @front = -1 
      @back  = -1 
    else 
      val = @buff[@back] 
      @back = (@back - 1) % @buff_size
    end 
    val  
  end 

  # Elimina el primer elemento 
  def shift?()
    return nil if self.empty? 
    if @front == @back 
      val = @buff[@front] 
      @front = -1
      @back = -1
    else 
      val = @buff[@front] 
      @front = (@front+1) % @buff_size
    end 
    val
  end 

  # Agrega un elemento al inicio 
  def unshift(val : T) 
    check_resize 
    if @front == -1 
      @front = 0
      @back = 0 
    elsif @front == 0
      @front = @buff_size - 1 
    else
      @front -= 1
    end 

    @buff[@front] = val
    self 
  end 

  # Regresa la cantidad de elementos 
  def size 
    ((@back - @front) % @buff_size) + 1
  end 

  # Acceder al i-esimo elemento 
  def [](index) 
    raise "index out of bouds" if self.empty? 
    index = (index + self.size) if index < 0 
    index = (@front + index) % @buff_size
    shiftindex = (index - @front) % @buff_size 
    raise "index out of bouds" unless 0 <= shiftindex <= ((@back-@front) % @buff_size)
    return @buff[index]
  end

  # Modificar el i-esimo elemento 
  def []=(index,val) 
    raise "index out of bouds" if self.empty? 
    index = (index + self.size) if index < 0 
    index = (@front + index) % @buff_size
    shiftindex = (index - @front) % @buff_size 
    raise "index out of bouds" unless 0 <= shiftindex <= ((@back-@front) % @buff_size)
    @buff[index] = val 
  end 

  private def check_resize 
    if ((@front % @buff_size) == ((@back+1) % @buff_size))
      aux_size = @buff_size 
      aux = Pointer(T).malloc(aux_size)
      ptr = @front
      index = 0 
      while(index < aux_size)
        aux[index] = @buff[ptr]  
        ptr = (ptr + 1) % @buff_size
        index += 1
      end
      @buff_size *= 2 
      @buff = @buff.realloc(@buff_size) 
      (0 ... aux_size).each do |k| 
        @buff[k] = aux[k] 
      end 
      @front = 0 
      @back  = aux_size-1
    end 
  end 
  
  def inspect(io) self.to_s(io) end 
  def to_s(io) 
      (0 ... @buff_size).each do |k| 
        kshift = (k-@front) % @buff_size 
        if !self.empty? && 0 <= kshift <= ((@back - @front) % @buff_size)
          io << "*" if k == @front 
          io << @buff[k] 
          io << "'" if k == @back 
        else 
          io << "/" 
        end 
        io << "," if k < @buff_size-1
      end 
  end 
end 




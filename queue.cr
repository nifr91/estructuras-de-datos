#!/usr/bin/env crystal 

# Is a FIFO (First in Firs Out) linear data structure, which means that 
# the element inserted first will be removed first. 
#
class Queue(T)

  private class Node(T) 
    property? :next,:prev
    @prev : Node(T) | Nil = nil 
    @next : Node(T) | Nil = nil 
    def initialize(val : T) 
      @val = val 
    end
    def val() @val end 
  end 

  @head : Node(T) | Nil 
  @tail : Node(T) | Nil 

  def initialize 
    @head = nil
    @tail = nil 
    @size = 0 
  end

  def push(val : T) 
    @size += 1 
    n = Node.new(val) 
    if t = @tail 
      t.next = n
      n.prev = t
      @tail = n 
    else
      @head = n 
      @tail = n 
    end 
    self 
  end 

  def empty?() @size == 0 end 
  def size() @size end 

  def next?() 
    return nil unless h = @head 
    h.val 
  end 

  def pop? 
    return nil if self.empty? 

    @size -= 1 
    if h = @head
      @tail = nil if @tail == h
      @head = h.next?
      @head.try{|h| h.prev = nil}
      h.next = nil
      h.val 
    end 
  end 

  def inspect(io) io << self end 
  def to_s(io) 
    n = @head 
    io << "*" 
    while n
      io << n.val 
      io << "," if n != @tail 
      n = n.next? 
    end 
    io << "'"
  end 

end 

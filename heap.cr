#!/usr/bin/env crystal 

# Un heap es una estructura de datos lineal (un arreglo) que 
# puede ser visualizado como un árbol binario casi completo (va 
# rellenando siempre por niveles de izquierda a derecha) 
#
# En el heap se cumple que cada elemento es mayor (o menor) que sus hijos, 
# puede representarse de dos formas, como un árbol o en un arreglo 
# 
# | Representación de árbol 
# | 
# | (16  
# |   (14 
# |     (8 
# |       (2 () ()) 
# |       (4 () ())) 
# |     (7 
# |       (1 () ())
# |       ())) 
# |   (10 
# |     (9 
# |      () 
# |      ()) 
# |     (3 
# |      () 
# |      ())))
# |
# 
# | Representación en arreglo 
# |  
# |                          +----------------------+ 
# |      +-+----+       +----|-----------+----+     |
# |      | |    |       |    |           |    |     |
# |      | v    v       |    |           v    v     v
# |  [ 16 | 14 | 10 |  8 |  7 |  9 |  3 |  2 |  4 |  1 |    |    |    ...    ]
# |           |    | ^    ^    ^    ^
# |           |    | |    |    |    | 
# |           +----|-+----+    |    |
# |                +-----------+----+
# 
# 
# Operaciones: 
# 
# * push -- inserta un elemento en el heap 
# * pop  -- elimina un elemento del heap
# * peek -- ver el elemento raíz. 
#

require "./vect" 

class Heap(T) 

  def initialize
    @buff = Vect(T).new(0)
    @size = 0
  end
 
  # Funciones para calcular la posición del padre o hijos en función 
  # de la posición del índice.
  private def left(i : Int32)  (i << 1) + 1 end 
  private def right(i : Int32) (i << 1) + 2 end 
  private def pa(i : Int32)    ((i-1) >> 1) end 

  # Mantener la propiedad del heap 
  private def heapify(i)
    return  if i < 0 
    n = @buff[i] 
    nexti = i 

    pi = pa i
    li = ((l = left i)  < @buff.size) ? l : -1 
    ri = ((r = right i) < @buff.size) ? r : -1
  
    if pi >= 0 && @buff[pi] < n 
      nexti = pi
    elsif ((ri >= 0) && (l >= 0)) && ((@buff[ri] > n) || (@buff[li] > n))
      nexti = (@buff[ri] > @buff[li]) ? ri : li
    elsif (li >= 0) && (@buff[li] > n) 
      nexti = li 
    elsif (ri >= 0) && (@buff[ri] > n)
      nexti = ri
    end 

    return if nexti == i 
    @buff[i],@buff[nexti] = @buff[nexti],@buff[i] 
    heapify nexti 
  end

  # Agregar un elemento al heap 
  def push(val : T)
    @buff.push(val)
    @buff
    heapify (@buff.size - 1)
    self 
  end 

  # Eliminar un elemento del heap 
  def pop?
    return nil if self.empty?
    n = @buff[0]
    @buff[0] = @buff[-1]
    @buff.pop?
    heapify 0 
    return n
  end 

  # Ver el elemento en la raíz 
  def peek?
    return nil if self.empty?
    @buff[0]
  end 

  # Regresa true si el heap está vacío 
  def empty?() @buff.size == 0 end 

  def inspect(io) io << self end 
  def to_s(io) io << @buff end 
end 

#!/usr/bin/env crystal 

require "./*" 
require "debug" 

def log(msg) print (msg.empty? ? "":'#'),' ',msg,'\n'  end 

log "Vector =================================================================="
log ""

log "Crear un arreglo de tamaño n "
pp! n = 10 
pp! ary = Vect(Int32).new(n) 
log "Agregar 3 elementos "
pp! ary.push(1),ary 
pp! ary.push(2),ary 
pp! ary.push(3),ary 
log "Tamaño del arreglo "
pp! ary.size 
log "Agregar 20 elementos, se debe redimensionar" 
pp! 20.times{|i| ary.push(i)}
pp! ary,ary.size 
log "Eliminar 22 elementos " 
pp! 22.times{ary.pop?} 
pp! ary,ary.size 
log "Indexado negativo" 
pp! 4.times{|i| ary.push(i)} 
pp! ary 
pp! ary[-1]
pp! ary[-2] 
pp! ary[-3]
pp! ary[-4]
log "Liberar memoria no utilizada " 
pp! ary.shrink

log ""
log "List =================================================================="
log ""

log "Crear una lista" 
pp! l = List(Int32).new 
log "Agregar tres elementos a la lista" 
{1,2,3}.each{|i| pp! l.push(i)} 
log "Agregar tres elementos al inicio de la lista" 
{4,5,6}.each{|i| pp! l.unshift(i)} 
log "Crear un iterador al inicio de la lista" 
pp! it = l.begin 
log "Avanzar el iterador dos posiciones" 
2.times{ pp! it.next?} 
pp! it
log "Insertar dos elementos en la posición del iterador" 
{7,8}.each{|i| pp! l.insert(it,i)} 
log "Avanzar hasta el final de la lista el iterador" 
pp! l
while (it.next?)
  pp! it 
end 
log "Retroceder tres posiciones mediante el iterador" 
3.times{ pp! it.prev? } 
log " Retroceder hasta el inicio de la lista el iterador" 
pp! l 
while (it.prev?) 
  pp! it 
end 
log "Avanzar tres posiciones" 
3.times{pp! it.next?} 
log "Eliminar el elemento en la posición del iterador" 
pp! l 
pp! l.erase(it) 
pp! l 
log "el iterador queda apuntando al nodo pero no se puede mover"
pp! it.next?
pp! it.prev?
pp! it
log "Eliminar dos elementos al final de la fila"
pp! l
2.times{ pp! l.pop}
pp! l
log "Eliminar dos elementos al inicio  de la fila"
pp! l
2.times{ pp! l.shift}
pp! l
log "Borrar todos los elementos usando el iterador"
pp! it = l.begin
pp! l 
while ((it = l.erase(it)).ptr?) 
  pp! l 
end 
pp! l 
log "Borrar todos los elementos usando el iterador" 
{1,2,3}.each do |i| l.push(i) end 
pp! it = l.end
pp! l 
while((it = l.erase(it)).ptr?) 
  pp! l 
end 
pp! l 

log ""
log "Deque ===================================================================="
log ""

log "Crear un deque "
pp! d = Deq(Int32).new
log "Agregar al final" 
{1,2,3}.each{|i| pp! d.push(i) } 
log "Agregar al inicio" 
{4,5,6}.each{|i| pp! d.unshift(i) } 
log "Agregar al final" 
{7,8,9}.each{|i| pp! d.push(i) }
log "Obtener el elemento al inicio" 
pp! d.front?
log "Obtener el elemento al final" 
pp! d.back?
log "Eliminar dos elementos atrás" 
pp! d 
2.times{ pp! d.pop?} 
pp! d 
log "Eliminar dos elementos adelante" 
pp! d 
2.times{ pp! d.shift?} 
pp! d 
log "Obtener el elemento i-esimo" 
pp! d.size 
pp! d[0] 
pp! d[1]
pp! d[2] 
pp! d[3] 
pp! d[4]
pp! d[-1] 
pp! d[-5] 
log "Modificar el elemento i-esimo" 
pp! d[0] = 100 
pp! d[2] = 0 
pp! d[-1] = 100 
pp! d


log ""
log "Stack ===================================================================="
log "" 

log "Crear un stack"
pp! s = Stack(Int32).new 
log "Agregar elementos al stack"
3.times {|i| pp! s.push(i)}
log "Ver el elemento activo en el stack" 
pp! s.top?
log "Eliminar 2 elementos" 
2.times{|i| pp! s.pop? } 
pp! s
log "Determinar si hay o no elementos" 
pp! s.empty? 
pp! s.pop?
pp! s.empty?
log "Agregar 10 elementos"
10.times{|i| pp! s.push(i)} 
log "Número de elementos en el stack"
pp! s.size 

log ""
log "Queue ===================================================================="
log "" 


log "Crear un queue" 
pp! q = Queue(Int32).new 
log "Agregar cuatro elementos" 
4.times{|i| pp! q.push(i)} 
log "Ver el siguiente elemento" 
pp! q.next? 
log "Eliminar tres elementos"
3.times{|i| pp! q.pop? } 
pp! q 
log "Ver el siguiente elemento" 
pp! q.next? 
log "Agregar cuatro elementos" 
4.times{|i| pp! q.push(i)} 
log "Eliminar tres elementos"
3.times{|i| pp! q.pop? } 
pp! q 

log ""
log "Heap ===================================================================="
log "" 

log "Crear un heap" 
pp! h = Heap(Int32).new 
log "Ver el mayor elemento" 
pp! h.peek? 
log "Agregar cuatro elementos al heap"
4.times{|i| pp! h.push(i)} 
log "Ver el mayor elemento " 
pp! h.peek? 
log "Eliminar 3 elementos" 
3.times{|i| pp! h.pop? } 
pp! h 
log "Ver el mayor elemento " 
pp! h.peek? 
log "Agregar valores repetidos" 
{2,3,4,2,1}.each{|i| pp! h.push(i)} 
log "Eliminar 3 elementos" 
3.times{|i| pp! h.pop? } 
pp! h 


log ""
log "Binary Search Tree ======================================================="
log ""


log "Crear un bst"
pp! t = Treap(Int32).new 
log "Agregar 6 elementos aletoreamente" 
10.times { |i| pp! t.push(i)} 
pp! t
log "Mínimo del árbol"
pp! t.min?
log "Máximo del árbol"
pp! t.max?
log "Eliminar los elementos 1 2 3" 
{1,2,3,9}.each{|i| pp! t.pop?(i)} 
pp! t 
log "Mínimo del árbol"
pp! t.min?
log "Máximo del árbol"
pp! t.max?

log ""
log "Set  ====================================================================="
log ""

log "Crear el set" 
t = TreapSet(Int32).new
log "Agregar elementos" 
[1,2,3,4,5,6].each do |i| pp! t.push(i) end 
pp! t 
log "Eliminar algunos elementos"
{1,3,9,8}.each do |k| pp! t.pop?(k) end 
pp! t 
log "Revisar si existe un elemento"
pp! t.includes?(1) 
pp! t.includes?(22)
pp! t.includes?(2)
pp! t 
log "Movernos mediante un iterador" 
pp! it = t.begin
pp! it.next?
pp! it.next? 
pp! it.prev?
pp! it.next?
pp! it.next?
pp! it.next?
pp! it = t.end
pp! it.prev?

log ""
log "Map  ====================================================================="
log ""



log "Crear un map" 
pp! m = Map(String,Int32).new
log "Número de elementos" 
pp! m.size 
log "Verificar si esta vacio"
pp! m.empty? 
log "Agregar valores al map"
pp! m["zz"] = 1
pp! m["z"] = 10
pp! m["y"] = 100
pp! m["x"] = -10 
log "Número de elementos" 
pp! m.size 
log "Verificar si esta vacio"
pp! m.empty? 
log "Obtener iterador" 
pp! it = m.begin
pp! it = m.end 
pp! it.prev?
pp! it.prev?
pp! it.prev?
pp! it.next?
pp! it.next?
log "Verificar si un nodo esta en el map"
pp! m["y"]? 
log "Eliminar un nodo" 
pp! m.pop?("y") 
log "Verificar si un nodo esta en el map"
pp! m["y"]? 
log "Eliminar un nodo que no esta" 
pp! m.pop?("y")
log "Modificar un valor" 
pp! m["x"] = 2
pp! m 
log "Eliminar todos los elementos"
pp! m.clear
pp! m

log ""
log "Updatable Heap  ========================================================="
log ""




log "Create a new updatable Heap" 
pp! uh = UpdatableHeap(String,Int32).new
log "Push a value" 
pp! uh["a"] = 1
pp! uh["b"] = 2
log "Get top priority" 
pp! uh.next?
log "Change priority" 
pp! uh["a"] = 10 
log "Get top priority" 
pp! uh.next?
log "Delete priority" 
pp! uh.pop?("b") 
pp! uh.next?
pp! uh["b"]=10
pp! uh["c"]=10
pp! uh["d"]=10
pp! uh["e"]=10
pp! uh["f"]=10
pp! uh.next?
pp! uh["x"]=10
pp! uh["g"]=11
pp! uh.next?
pp! uh.next?.try{|e| pp! e.key ; uh.pop?(e.key)} 
while pair = uh.pop? 
  pp! pair
end 

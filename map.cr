#!/usr/bin/env crystal 

# La plantilla map permite crear contenedores asociativos formados por un 
# tipo clave y un tipo al que se mapea llamado valor. 
#
# La complejidad algorítmica asociada a las diferentes operaciones es igual 
# a la del set, al igual que este se emplean árboles balanceados. 
#
# Los métodos son similares a los del set, pero en este caso 
# para buscar un elemento se emplea la clave. Además esta definido el operador 
# [] que recibe la clave. 
#

class Map(K,V) 

  @root : Node(K,V) | Nil 
  def initialze(@root = nil)  end 

  # Regresa el tamaño del 
  def size()  (r = @root)? r.cnt+1 : 0 end
  def begin() Iterator(K,V).new(min?(@root)) end
  def end ()  Iterator(K,V).new(max?(@root)) end
  def empty?() !@root  end
  def clear() @root = nil end

  def []=(key,val)
    if n = find(key,@root)
      n.val = val 
      return self 
    end 
    node = Node(K,V).new(key,val) 
    l,m = split?(@root,key) 
    m,r = split?(m,key,leq: false)     
    @root = merge?(merge?(l,node),r)
    self 
  end

  def pop?(key)      
    l,m = split?(@root,key) 
    m,r = split?(m,key,leq: false) 
    @root = merge?(l,r) 
    (m) ? m.val : m
  end

  def []?(key)
    (n = find(key,@root))? n.val  : nil 
  end


  private class Iterator(K,V) 
    def initialize(@node : Node(K,V)| Nil) end 

    def next?()  (@node = succ?(@node)) ? self : nil end
    def prev?() (@node = pred?(@node)) ? self : nil end
    def val?() @node end 

    private def succ?(node) 
      return node if !node 
      if node.r? 
        return min?(node.r?) 
      end 
      p = node.p? 
      while p && node == p.r?
        node = p 
        p = p.p? 
      end 
      p 
    end

    private def pred?(node)
      return node if !node 
      if node.l? 
       return max?(node.l?) 
      end 
      p = node.p?
      while p && node == p.l? 
        node = p 
        p = p.p?
      end 
      p
    end 

    private def min?(node)
      if node && node.l? 
        min?(node.l?) 
      else 
        node
      end 
    end 

    private def max?(node) 
      if node && node.r? 
        max?(node.r?) 
      else 
        node
      end 
    end 
    def inspect(io) io << self end 
    def to_s(io) io << @node end 
  end 
 


  private class Node(K,V) 
    @id : UInt64 = rand(UInt64)
    @l  : Node(K,V) | Nil
    @r  : Node(K,V) | Nil
    @p  : Node(K,V) | Nil
    @cnt : Int32 = 0
    property? l,r,p 
    property val
    getter key,id,cnt
    def initialize(@key : K,@val : V) 
    end
    def update
      @cnt = 0
      @l.try{|l| @cnt += l.cnt+1 } 
      @r.try{|r| @cnt += r.cnt+1 } 
    end 
    def to_s(io) io << "(" << @key << "=>" << @val << ")" end 
  end 

  private def min?(node) (node && node.l?) ? min?(node.l?) : node end 
  private def max?(node) (node && node.r?) ? max?(node.r?) : node end 

  private def find(key,node) 
    if !node || key == node.key 
      node 
    elsif key < node.key
      find(key,node.l?) 
    else 
      find(key,node.r?) 
    end 
  end 

  private def merge?(tl,tr) 
    return ((tl) ? tl : tr) if !tl || !tr
    if tl.id > tr.id
      tl.r = merge?(tl.r?,tr)
      tl.r?.try{|tlr| tlr.p = tl} 
      tl.update 
      tl
    else 
      tr.l = merge?(tl,tr.l?) 
      tr.l?.try{|trl| trl.p = tr} 
      tr.update
      tr 
    end 
  end 

  private def split?(node,key : K,leq = true) 
    tl = tr = nil 
    return tl,tr if !node
    comp = (leq) ? (key <= node.key) : (key < node.key)
    if comp
      tr = node
      l,r = split?(tr.l?,key,leq)
      tr.l = r
      r.try{|r| r.p = tr}
      l.try{|l| l.p = nil} 
      tl = l
    else 
      tl = node 
      l,r = split?(tl.r?,key,leq) 
      tl.r = l
      l.try{|l| l.p = tl} 
      r.try{|r| r.p = nil} 
      tr = r
    end 
    node.update
    return tl,tr
  end 

  def inspect(io) io << self end 
  def to_s(io) 
    it = self.begin
    io << it.val? 
    while it.next?
      io << " " << it.val?
    end 
    io 
  end 
end 

#!/usr/bin/env crystal 

# Plantilla que permite disponer de listas doblemente enlazadas
#
class List(T) 
  private class Node(T)
    @val : T
    @prev : Node(T)| Nil = nil 
    @next : Node(T)| Nil = nil 
    def initialize(@val : T) end 
    def val() @val end
    property? :next, :prev, :val 
  end 

  @head : Node(T) | Nil = nil
  @end  : Node(T) | Nil = nil 

  # Devuelve el contenido del primer elemento 
  def front() @head.val  end 

  # Devuelve el contenido del último elemento 
  def back()  @end.val  end 

  # Agrega al final de la lista 
  def push(val : T) 
    n = Node(T).new(val) 
    n.prev = @end
    if e = @end 
      e.next = n
    else
      @head = n 
    end 
    @end = n
    return self 
  end 

  # Elimina el último elemento de la lista 
  def pop
    return nil unless n = @end
    @end = n.prev? 
    @end.try{|e| e.next = nil} 
    n.prev = nil 
    return n.val if n 
    nil 
  end

  # Elimina el primer elemento de la lista 
  def shift
    return nil unless n = @head
    @head = n.next?
    @head.try{|e| e.prev = nil} 
    n.next = nil 
    return n.val if n 
    nil 
  end 
  
  # Agrega un elemento al inicio de la lista 
  def unshift(val : T) 
    n = Node(T).new(val) 
    n.next = @head
    if h = @head 
      h.prev = n
    else 
      @end = n 
    end 
    @head = n 
    return self  
  end
  
  # Iterador al primer elemento 
  def begin() Iterator.new(@head) end

  # Iterador al último elemento 
  def end()  Iterator.new(@end) end 

  # Insertar un elemento en O(1) -> esta es una ventaja frente al vector, 
  # claro que de alguna forma se debe tener acceso a los iteradores (muchas
  # están en árbol para poder acceder a ellos rápidamente) 
  #
  def insert(itpos : Iterator, val : T)
    raise "invalid iterator" unless pos = itpos.ptr?
    n = Node(T).new(val)
    n.prev = pos 
    n.next = pos.next?
    pos.next?.try{|pn| pn.prev = n} 
    pos.next = n
    return self 
  end 

  # Elimina el nodo en la posición del iterador, una vez borrado, ese iterador
  # ya no se puede usar. La función erase devuelve un iterador al 
  # siguiente elemento, así que se puede reasignar. 
  def erase(itpos : Iterator) 
    raise "iterator out of bounds" unless pos = itpos.ptr?
    next_pos = pos.next? 
    next_pos = next_pos ? next_pos : pos.prev?
    @head = pos.next? if pos == @head 
    @end  = pos.prev? if pos == @end
    pos.prev?.try{|pp| pp.next = pos.next? } 
    pos.next?.try{|pp| pp.prev = pos.prev? } 
    pos.next = nil 
    pos.prev = nil
    return Iterator.new(next_pos)
  end 

  def inspect(io) to_s(io) end 
  def to_s(io) 
    io << "("
    if n = @head
      while n
        io  << n.val  
        io << " -> " if n.next?
        n = n.next?
      end 
    end 
    io << ")"
  end 

  private class Iterator(T)
    def initialize(@ptr : Node(T) | Nil) end 
    def ptr?() @ptr end 
    def next?
      return nil unless ptr = @ptr 
      if ptr = ptr.next?
        @ptr = ptr
        self 
      else 
        nil 
      end 
    end 
    def prev? 
      return nil unless ptr = @ptr 
      if ptr = ptr.prev?
        @ptr = ptr 
        self 
      else 
        nil 
      end 
    end 
    def inspect(io) to_s(io) end 
    def to_s(io) @ptr.try{|ptr| io << ptr.val}  end 
  end 
end 



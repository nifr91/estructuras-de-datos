#!/usr/bin/env crystal 

# Priority queue with updatable priorities
#
# Supongamos que queremos desarrollar un heap en el que los nodos 
# tienen un identificador único, además de su prioridad. Queremos tener la
# posibilidad de borrar y/o actualizar (sumar,restar o cambiar un valor 
# a la prioridad) de un nodo (a partir de su id) de forma eficiente (O(lg(n))) 
#
class UpdatableHeap(K,P)

  struct Element(K,P)
    getter key,priority
    def initialize(@key : K,@priority : P)
    end 
    def <(other : Element) 
      if @priority == other.priority 
        @key < other.key 
      else 
        @priority < other.priority
      end
    end 
    def >(other : Element)  !(self < other)  end 
    def ==(other : Element) !(self < other) && !(other < self) end 
    def !=(other : Element) !(self == other) end
    def <=(other : Element) (self < other) || (other == self) end
    def >=(other : Element) (self > other)|| (other == self) end
    def inspect(io) io << self end 
    def to_s(io) io << "(" << @key << " => " << @priority << ")" end 
  end 

  # La estructura consiste en un buffer que almacena un heap y una mapa 
  # que almacena el índice de un valor
  @buff = Vect(Element(K,P)).new
  @map  = Map(K,Int32).new

  # Permite calcular el índice del nodo hijo izquierdo , regresa -1 si no existe
  private def left(i)   
    l = (i << 1) + 1
    (l < @buff.size) ?  l : -1 
  end 
  # Permite calcular el índice del nodo hijo derecho, regresa -1 si no existe 
  private def right(i)  
    r = (i << 1) + 2 
    (r < @buff.size) ? r : -1
  end 

  # Permite calcular el índice del nodo padre, regresa -1 si no existe
  private def parent(i) (i-1) >> 1 end 
 
  # Se encarga de eliminar un valor del heap, recibe como argumento la etiqueta
  # del valor a eliminar. Si no se asigna se elimina el valor con la prioridad 
  # más alta.
  #
  # Regresa 'nil' si no se encuentra la etiqueta, o la cola esta vacía. 
  def pop?(key = nil)
    return nil if @buff.empty?
    key = @buff[0].key unless key 
    return nil unless index = @map.pop?(key)
    @buff.swap(index,-1)
    pair = @buff.pop?
    if index < @buff.size
      @map[@buff[index].key] = index 
      # heapify_down(index)
      heapify(index) 
    end 
    return pair
  end 

  # Regresa el valor con la prioridad más alta 
  def next?
    return nil if @buff.empty?
    @buff[0]
  end 

  # Asigna el valor de prioridad a la etiqueta dada, si la etiqueta ya se 
  # encontraba se modifica su valor, si no estaba se agrega
  def []=(key : K ,priority : P)
    self.pop?(key) if index = @map[key]?
    index = @buff.size 
    @buff.push(Element.new(key,priority))
    @map[key] = index
    # heapify_up(index)
    heapify(index) 
    self 
  end 

  # Verifica si una etiqueta está el la cola, en caso de que este se regresa
  # su prioridad y si no esta se regresa 'nil'
  def []?(key : K)
    if index = @map[key]? 
      @buff[index][1]
    end 
  end

  # ===========================================================================
  # Funciones para mantener la propiedad del heap 
  # ===========================================================================

  # Se mantiene la propiedad del heap, si el padre es menor, el hijo debe 
  # subir, y si alguno de los hijos es mayor , sel nodo debe bajar en el 
  # heap.
  #
  # Note que se delega la comparación al objeto nodo, en nuestro caso, 
  # el nodo es a < b si a.p < b.p o en caso de que a.p == b.p entonces 
  # so a.k < b.k. Esto mantiene la propiedad del heap, permitiendo regresar
  # la clave de mayor precedencia en caso de que tengan la misma prioridad.
  #
  private def heapify(i) 
    val = @buff[i]
    nxi = i 
    pi = parent i 
    ri = right i 
    li = left  i

    if (pi >= 0) && @buff[pi] < val
      nxi = pi  
    elsif (li >= 0) && (ri >= 0) 
      index = (@buff[li] > @buff[ri]) ? li : ri 
      nxi = (@buff[index] > val) ? index  : nxi 
    elsif (li >= 0) && ((lval = @buff[li]) >= val)
      nxi = li
    elsif (ri >= 0) && ((rval = @buff[ri]) >= val) 
      nxi = ri
    end 

    return if nxi == i 
    swap(nxi,i)
    heapify(nxi) 
  end 


  # Intercambia dos valores en el arreglo, actualizando sus índices en el 
  # map. 
  private def swap(ai,bi)
    @map[@buff[ai].key] = bi
    @map[@buff[bi].key] = ai
    @buff.swap(ai,bi) 
  end 
 
  # IO ------------------------------------------------------------------------

  def inspect(io) io << self end 
  def to_s(io) io << @buff end 

  # Clases auxiliares ---------------------------------------------------------

  # Clase vector para manejar push,pop y acceso a los elementos 
  private class Vect(T)
    @buff = Pointer(T).malloc(2)
    @buff_size = 2
    @size = 0 
    def push(val : T)
      check_resize
      @buff[@size] = val 
      @size += 1 
      self 
    end 
    def pop? 
      return nil if self.empty?
      @size -= 1
      @buff[@size]
    end 
    def size() @size end 
    def empty?() @size == 0 end 
    def [](index) return @buff[check_bounds(index)] end 
    def []=(index,val) @buff[check_bounds(index)] = val end 
    def swap(a,b)
      a = check_bounds(a) 
      b = check_bounds(b) 
      @buff[a],@buff[b] = @buff[b],@buff[a] 
    end 
    private def check_bounds(index) 
      index = @size + index if index < 0
      raise "index: #{index} out of bounds size: #{@size}" unless 0 <= index < @size 
      index
    end 
    private def check_resize
      return if @size < @buff_size
      @buff_size *= 2 
      @buff = @buff.realloc(@buff_size)
    end 
    def inspect(io) io << self end 
    def to_s(io) 
      (0 ... @size).each do |i| 
        io << @buff[i] 
        io <<  "," unless (i == (@size-1)) 
      end 
      io 
    end 
  end 


  # Clase mapa, se utiliza para mapear las (K) claves (llaves) a los índices 
  # (V) en el buffer 
  private class Map(K,V)
    # Clase nodo para generar el árbol binario de búsqueda balanceado 
    private class Node(K,V)
      @id  : UInt64  = rand(UInt64::MAX)
      @l   : Node(K,V) | Nil 
      @r   : Node(K,V) | Nil 
      property? l,r
      property val
      getter key,id
      def initialize(@key : K,@val : V) end
    end 

    @root  : Node(K,V) | Nil = nil 
    private def split?(node,key,leq=true) 
      ml = mr = nil 
      return mr,ml unless node 
      comp = (leq) ? (key <= node.key) : (key < node.key) 
      if comp 
        mr = node 
        ml,mr.l = split?(node.l?,key,leq)
      else 
        ml = node 
        ml.r,mr = split?(node.r?,key,leq)
      end 
      return ml,mr 
    end 

    private def merge?(ml,mr)
      return (ml)? ml : mr if !ml || !mr
      if ml.id < mr.id 
        mr.l = merge?(ml,mr.l?)
        mr 
      else 
        ml.r = merge?(ml.r?,mr)
        ml 
      end 
    end

    private def find?(node,key : K)
      return node if !node || node.key == key 
      return find?((key < node.key) ? node.l? : node.r?,key) 
    end 

    def []?(key : K)
      return nil unless node = find?(@root,key) 
      return node.val
    end

    def []=(key : K, val : V)
      if node = find?(@root,key) 
        node.val = val
        return self
      end 

      node = Node.new(key,val)
      l,r = split?(@root,key)
      @root = merge?(merge?(l,node),r)
      self 
    end 

    def pop?(key : K) 
      l,m = split?(@root,key)
      m,r = split?(m,key,leq: false)
      @root = merge?(l,r)
      (m) ? m.val : m 
    end 

    def inspect(io) io << self end 
    def to_s(io) inorder(@root,io) end 
    private def inorder(node,io) 
      return io unless node 
      inorder(node.l?,io) 
      io << ({node.key,node.val}) <<","
      inorder(node.r?,io) 
      io 
    end 
  end 
end 

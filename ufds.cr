#!/usr/bin/env crystal 

# Ricardo Nieto Fuentes
# nifr91@gmail.com
#
# Union Find Disjoint Set 
#
# Es una estructura de datos para modelar datos que inicialmente son disjuntos 
# con la habilidad de hacer las siguientes operaciones de forma eficiente: 
#
# * Comprobar si dos elementos pertenecen al mismo conjunto
# * Unir dos conjuntos
# * Obtener el tamaño del un conjunto 
#
# La idea clave en el ufds es mantener para cada conjunto un elemento 
# representante. Se forma árboles con los nodos de un grupo y la 
# raíz es el representante. El padre de cada uno se almacena en un vector. 
# 
#
class UnionFindDS(T)

  # Clase nodo que contiene el rango y el número de elementos en el conjunto,
  # si es el representante. Así como el padre del nodo en caso de que no 
  # sea el representante. 
  #
  private class Node(T)
    @rank = 0 
    @cnt = 1
    @parent : Node(T)
    property val,pa,rank,cnt,parent
    def initialize(@val : T,@parent = self) end
    def to_s(io) io << "(" << @val << "=>" << @parent.val << ")" end 
    def inspect(io) io << self end 
  end 

  # Se almacena en un mapa
  @map = Map(T,Node(T)).new

  # Se inserta en el conjunto al agregar al mapa el valor 
  def push(val : T) 
    node = Node.new(val)
    @map[val] = node
    val 
  end 

  # Saber si pertenecen al mismo conjunto 
  def sameset?(a : T, b : T) 
    find(a)  == find(b)
  end 

  # Buscar el representante del conjunto al que pertenece el valor 'a'.
  # Empleamos la compresión de camino para reducir la altura del árbol. 
  #
  def find(a : T)
    raise "#{a} not in set" unless n = @map[a]?
    n.parent = find(n.parent.val) if n.parent != n
    return n.parent
  end

  # Combinar dos conjuntos, regresa el número de nodos en el nuevo conjunto
  # se emplea el 'rank' para disminuir el coste computacional.  
  def merge(a : T, b : T) 
    # Encontrar el representante de cada conjunto
    ra = find(a)
    rb = find(b)
  
    # Si pertenecen al mismo conjunto no hay nada que realizar.
    return ra.cnt if ra == rb  

    # Si ambos conjuntos tienen el mismo rango se aumenta el rango del 
    # representante , si no tiene el mismo rango, se elije el de mayor 
    # rango como nuevo representante para no aumentar el tamaño del árbol. 
    p,c = if ra.rank ==  rb.rank
      ra.rank += 1
      {ra,rb}
    elsif ra.rank > rb.rank 
      {ra,rb}
    else 
      {rb,ra}
    end 
    
    # El hijo (menor rango) tiene como padre al de mayor rango (para 
    # no aumentar la altura). También se actualiza el número de nodos 
    # en el conjunto.
    c.parent = p 
    p.cnt += c.cnt
  end 

  # Para determinar si esta en el conjunto se delega a la función map. 
  def []?(a : T) @map[a]?  end 

  def inspect(io) io << self end 
  def to_s(io) io << @map end 

  # ===========================================================================

  # Clase que representa un mapa mediante un árbol binario aleatorio 
  private class Map(K,V)
    private class Node(K,V) 
      @id : UInt64 = rand(UInt64) 
      @l : Node(K,V) | Nil = nil 
      @r : Node(K,V) | Nil = nil 
      @p : Node(K,V) | Nil = nil 
      property? l,r,p
      getter key,val,id
      def initialize(@key : K, @val : V) end 
      def inspect(io) io << self end 
      def to_s(io) io << "(" << @key << "=>" << @val << "|" << @p << ")" end  
    end 

    @root : Node(K,V) | Nil = nil 

    def []=(key : K, val : V)
      n = Node.new(key,val)
      l,m = split?(@root,key)
      m,r = split?(m,key,leq: false)
      @root = merge?(merge?(l,n),r)
      val
    end

    def []?(key : K)
      n = @root 
      while n 
        return n.val if key == n.key
        n = (key < n.key)? n.l? : n.r?
      end 
      nil
    end

    def delete?(key : K) 
      l,m = split?(@root,key)
      m,r = split?(m,key,leq: false)
      @root = merge?(l,r) 
      m ? m.val : nil 
    end 

    private def merge?(l,r)
      return (l) ? l : r if !l || !r 
      if l.id > r.id
        l.r = merge?(l.r?,r)
        l.r?.try{|lr| lr.p = l}
        l
      else 
        r.l = merge?(l,r.l?) 
        r.l?.try{|rl| rl.p = r}
        r
      end 
    end 

    private def split?(n,key : K,leq = true)
      ml = mr = nil 
      return ml,mr unless n
      
      if ((leq) ? (key <= n.key) : (key < n.key))
        mr = n
        ml,mr.l = split?(mr.l?,key,leq)
        mr.l?.try{|l| l.p = mr}
        ml.try{|ml| ml.p = nil} 
      else 
        ml = n
        ml.r,mr = split?(ml.r?,key,leq)
        ml.r?.try{|r| r.p = ml} 
        mr.try{|mr| mr.p = nil} 
      end 
      return ml,mr
    end

    def each
      nx = min?(@root)
      while nx
        yield ({nx.key,nx.val})
        nx = succ?(nx)
      end 
    end 

    private def succ?(node)
      return nil unless node 
      return min?(node.r?) if node.r?
      p = node.p?
      while p && (node == p.r?)
        node = p 
        p = p.p?
      end 
      return p 
    end
    
    def min?(node) 
      return nil unless node
      while n = node.l?
        node = n
      end 
      return node
    end 

    def to_s(io) print_tree(@root,io) end 
    def inspect(io) io << self end 
    private def print_tree(node,io) 
      return io unless node
      print_tree(node.l?,io) 
      io << node.val
      io << " "
      print_tree(node.r?,io)
      return io 
    end 

  end 
end 

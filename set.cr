#!/usr/bin/env crystal 

#  Set 
#  Es un contenedor de datos que permite inserción, borrado y búsqueda en 
#  O(lg(n)), es decir igual que un árbol balanceado, muchas implementaciones
#  emplean un árbol balanceado. 
#
#  Los datos quedan ordenados, con lo que se puede realizar un recorrido de 
#  los datos en orden en O(n). 
#
#  Requiere que se definan los operadores de comparación
#
#  Métodos importantes 
#
#  * begin
#  * end 
#  * push
#  * count
#  * pop
#  * clear
#  * size
#
class TreapSet(T)


  @root : Node(T) | Nil 

  def initialize() @root = nil end


  # Para agregar un elemento se divide en dos el árbol en base al valor 
  #
  # l := {x | x < val} 
  # r := {x | x > val} 
  # m := val 
  #
  # después se une 'l' con un nuevo nodo 'm' y 'r'
  #
  # Si el valor ya se encuentra en el árbol solo se aumenta el contador 
  # que indica el número de veces que se encuentra repetido.
  #
  def push(val : T)
    n = Node.new(val)
    tl,tr = split?(@root,val)
    @root = merge?(merge?(tl,n),tr)
    self 
  end 

  # De manera análoga a la función push, se divide en tres partes el 
  # árbol,
  #
  # l := {x | x < val} 
  # m := {x | x == val} 
  # r := {x | x > val} 
  #
  # de forma que eliminar el valor solo consiste ignorar m al volver a unir
  # l y r. 
  #
  # Cuando un elemento esta repetido, se disminuye el número de veces que 
  # esta en el árbol, solo se elimina cuando la última copia es eliminada.
  #
  # /!\ Es importante notar que se libera la memoria del nodo, lo que 
  # invalida cualquier referencia que se tenga a el. 
  #
  # Note que dos intervalos a,b, el intervalo b es mayor  (a < b) si 
  # a.beg < b.beg o si a.beg == b.beg && a.end < b.end, de esta el 
  # sucesor de un intervalo 'r' es (r.beg,r.end+1), pues es el valor más
  # pequeño superior a 'r'.
  # 
  def pop?(val : T) 
    tl,m = split?(@root,val)
    m,tr = split?(m,val,false)
    @root = merge?(tl,tr)
    (m ? m.val : m)
  end 

  # Determina si un valor esta en el set o no 
  def includes?(val : T) !!find?(val) end 

  # Regresa un iterador que permite iterar los elementos del set en orden
  # ascendente con complejidad  O(n), regresa el iterador apuntando al primer
  # elemento (el de mínimo valor) 
  def begin() Iterator(T).new(self.min?) end

  # Regresa un iterador que permite iterar los elementos del set en orden
  # ascendente con complejidad  O(n), regresa el iterador apuntando al último 
  # elemento (el de máximo valor) 
  def end() Iterator(T).new(self.max?) end 



  # Clase que permite iterar el árbol en O(n) 
  private class Iterator(T)

    def initialize(@node : Node(T) | Nil)
    end 

    # Pasa al siguiente nodo 
    def next?
      @node  = succ?(@node)
      return (@node ? self : nil) 
    end

    def val
      @node.not_nil!.val 
    end 
 
    # Regresa al nodo anterior 
    def prev?
      @node = pred?(@node)
      return (@node ? self : nil) 
    end 

    # Encuentra el sucesor de un nodo de forma recursiva ( mínimo valor 
    # más grande que el nodo actual)
    def succ?(node)
      return nil unless node 
      if r = node.r?
        while r
          node = r 
          r = r.l? 
        end 
        node 
      else
        p = node.p?
        while p && (node == p.r?)
          node = p
          p = p.p?
        end 
        p
      end 
    end 

    # Encuentra el predecesor de un nodo de forma recursiva (máximo valor 
    # más chico que el nodo actual ) 
    def pred?(node) 
      return nil unless node
      if l = node.l? 
        while l 
          node = l 
          l = l.r? 
        end 
        node 
      else 
        p = node.p? 
        while p && (node == p.l?) 
          node = p 
          p = p.p?
        end 
        p
      end 
    end 

    def inspect(io) io << self end 
    def to_s(io) io << @node end 
  end 


  # Nodo que contiene los valores 
  private class Node(T) 
    @id : UInt64 = rand(UInt64::MAX) 
    @val : T 
    @cnt : Int32 = 0
    @l : Node(T) | Nil = nil  
    @r : Node(T) | Nil = nil 
    @p : Node(T) | Nil = nil 
    property? l,r,p
    getter val,id,cnt
    def initialize(@val : T) end 
    def to_s(io) io << "(" <<@val << ")"  end 
    def inspect(io) io << self end 
    def update
      @cnt = 0 
      @l.try{|l| @cnt+= l.cnt+1} 
      @r.try{|r| @cnt+= r.cnt+1} 
      @cnt 
    end 
  end 

  # Valor mínimo en el árbol 
  private def min?(n = @root) (n && n.l?) ? self.min?(n.l?) : n end 

  # Valor máximo en el árbol 
  private def max?(n = @root) (n && n.r?) ? self.max?(n.r?) : n end 


  # La función merge? se encarga de unir dos árboles, se asume que se tienen 
  # dos treaps, y cada treap cumple las propiedades en etiquetas y valores.
  # De esta forma para los valores de los árboles  se cumple que 
  # (∀x ∈ l) ∧ (∀y ∈ r) , x < y. 
  #
  # De forma que solo es necesario considerar las etiquetas, se tienen dos casos
  #
  # | (l
  # |   (ll (...) (...))
  # |   (lr (...) (...))) 
  # | (r 
  # |   (rl (...) (...))
  # |   (rr (...) (...))
  # 
  # * l.id > r.id en cuyo caso l debe ser el nodo raíz de r
  #
  #   | (l 
  #   |  (ll (...) (...))
  #   |  (merge (lr r))) 
  #
  # * l.id < r.id en cuyo caso r debe ser el nodo raíz de l 
  #
  #   | (r 
  #   |  (merge (l rl))
  #   |  (rr (...) (...))) 
  #
  #
  private def merge?(tl : Node(T) | Nil,tr : Node(T) | Nil)
    return (tl ? tl : tr) if !tl || !tr
    if tl.id > tr.id 
      tl.r = merge?(tl.r?,tr)
      tl.r?.try{|tlr| tlr.p = tl} 
      tl.update
      return tl
    else 
      tr.l = merge?(tl,tr.l?)
      tr.l?.try{|trl| trl.p = tr } 
      tr.update
      return tr
    end 
  end 


  # La función split se encarga de separar el treap en dos árboles 
  # 
  # l := {x ∈ T | x < val } 
  # r := {x ∈ T | x ≥ val } 
  # 
  # Para ello cada nodo se agrega a cada conjunto. 
  # 
  # | (n
  # |  (nl
  # |    (nll () ())
  # |    (nlr () ()))
  # |  (nr 
  # |    (nrl () ())
  # |    (nrr () ()))) 
  #
  # si el valor es menor o igual que el nodo actual, sabemos que el nodo 
  # y su lado derecho pertenecen al r. 
  #
  # | val <= n.val  
  # |
  # | r = (n
  # |       (? )
  # |       (nr (...)) 
  #
  # Resta determinar que elementos del lado izquierdo 'nl' pertenecen a 'r' y
  # cuales a 'l', para ello se llama recursivamente a la función. 
  # 
  # | l,rl = (split nl)
  #
  # de forma simétrica cuando el valor es mayor que el nodo actual sabemos 
  # que el nodo y si lado izquierdo pertenecen a 'l' y solo queda determinar 
  # que nodos del lado derecho pertenecen también a 'l'. 
  #
  # | n.val < val 
  # |
  # | l = (n
  # |       (nl (...))
  # |       (?) 
  #
  # | lr,r = (split nr)
  #
  #
  # En cada paso del split se actualiza el valor 'max_end'. 
  #
  private def split?(node : Node(T) | Nil,val : T,leq = true) 
    tr = tl = nil 
    return tl,tr if !node
  
    comp = (leq) ? (val <= node.val) : (val < node.val) 
    node.p = nil 
    if (comp)
      tr = node
      l,r = split?(tr.l?,val,leq)
      tr.l = r
      r.try{|r| r.p = tr} 
      l.try{|l| l.p = nil} 
      tl = l
    else 
      tl = node 
      l,r = split?(tl.r?,val,leq) 
      tl.r = l
      l.try{|l| l.p = tl} 
      r.try{|r| r.p = nil} 
      tr = r
    end 
    node.update 
    return tl,tr
  end 


  # Encontrar el nodo con el valor especificado, si 'val' == 'nodo'
  private def find?(val,node = @root)
    if !node || val == node.val
      node 
    elsif val < node.val
      find?(val,node.l?) 
    else 
      find?(val,node.r?)
    end
  end

  # El predecesor es el máximo valor en el árbol que es inferior a 'val'. 
  # Para encontrarlo, empezando de la raíz, si 'val' > 'nodo' actual
  # significa que el nodo es candidato y el siguiente predecesor debe estar 
  # a la derecha del nodo, por otro lodo si es inferior el predecesor estará
  # a la izquierda.
  #
  def pred?(val : T, node = @root)
    pred = nil 
    while node 
      if val > node.val 
        pred = node 
        node = node.r?
      else
        node = node.l? 
      end 
    end 
    pred
  end 

  # El sucesor es el valor mínimo en el árbol que es inferior a 'val'. 
  # Para encontrarlo, empezando de la raíz, si 'val' < 'nodo' significa que 
  # el nodo es candidato y el siguiente sucesor debe estar a la izquierda del 
  # nodo, por otro lado si es superior el sucesor estará a la derecha de ese 
  # nodo.
  #
  def succ?(val : T,node = @root)
    succ = nil
    while node
      if val < node.val
        succ = node
        node = node.l?
      else 
        node = node.r? 
      end 
    end 
    succ
  end 

  def inspect(io) io << self end 
  
  def to_s(io)
    it = self.begin 
    io << it.val 
    while it.next? 
      io << " " << it.val 
    end 
    io 
  end 
  
end 




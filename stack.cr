#!/usr/bin/env crystal 

# Stack is a linear data structure which follows a particular order in which 
# the operations are performed. Te order may be LIFO (Last In First Out) 
# or FILO (First In Last Out) 

class Stack(T)
  def initialize 
    @size      = 0
    @buff_size = 2
    @buff      = Pointer(T).malloc(@buff_size)
  end 

  # Inserta un elemento 
  def push(val : T) 
    check_resize
    @buff[@size] = val 
    @size += 1
    self 
  end

  # Elimina un elemento 
  def pop?
    return nil if self.empty?
    @size -= 1 
    @buff[@size] 
  end 
 
  # Número de elementos en la fila 
  def size() @size end 

  # Devuelve true si el número de elementos es 0 
  def empty?() @size == 0 end 

  # Devuelve el elemento activo de la pila
  def top? 
    return nil if self.empty? 
    @buff[@size-1] 
  end 


  private def check_resize
    if @size == @buff_size 
      @buff_size *= 2
      @buff = @buff.realloc(@buff_size)
    end
  end 

  def inspect(io) io << self end 
  def to_s(io)
    (0 ... @size).each do |i|
      io << @buff[i] 
      io << ", " if i < (@size - 1) 
    end 
    io << "|" 
  end 
end 





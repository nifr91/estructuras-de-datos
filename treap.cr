#!/usr/bin/env crystal 

# 
# Un árbol de búsqueda binario es un árbol binario que cumple la propiedad
# siguiente: 
#
# Para cualquier nodo, todos los elementos de su sub-árbol izquierdo son
# menores o iguales que el propio nodo. 
#
# Para mantener la complejidad O(lg(n)) el árbol binario debe estar balanceado
# esto es que las alturas de los dos subárboles de cualquier nodo difiere a lo
# sumo en 1. 
#
# Un tipo de árbol binario balanceado es el Treap, en el cual, que aunque no
# garantiza tener una altura ∈ O(lg(n)), tiene un comportamiento amortizado de
# ese orden. La idea es utilizar aleatorización y la propiedad del Heap; para
# mantener el balance con una alta probabilidad. Su complejidad esperada de
# búsqueda, inserción y borrado es ∈ O(lg(n)). 
# 
# Cada nodo debe mantener dos propiedades
# 
# * valor -- permite ordenar los datos como un árbol de búsqueda binario 
# * id    -- etiqueta asignada de forma aleatoria con la que se mantendrá 
#            la propiedad del heap.
# 
# La idea principal del treap es basarse en que la altura esperada de un árbol
# binario aleatorio es O(lg(n)), para simular esto, el treap genera el 'id' de
# forma aleatoria lo que provoca de forma indirecta una aleatorización del 
# árbol.
# 
# Las operaciones básicas del treap , al igual que el árbol binario son: 
# 
# * find -- buscar un elemento por su valor 
# * pop  -- borrar un valor en el árbol 
# * push -- agregar un elemento al árbol 
# 
#
class Treap(T) 
  
  # Nodo que contiene los valores 
  private class Node(T) 
    @id : UInt64 = rand(0u64...UInt64::MAX) 
    @val : T 
    @l : Node(T) | Nil = nil  
    @r : Node(T) | Nil = nil 
    property? l,r
    getter val,id
    def initialize(@val : T) end 
    def to_s(io) io << @val  end 
    def inspect(io) io << self end 
  end 

  @root : Node(T) | Nil 

  def initialize() @root = nil end


  # La función merge? se encarga de unir dos árboles, se asume que se tienen 
  # dos treaps, y cada treap cumple las propiedades en etiquetas y valores.
  # De esta forma para los valores de los árboles  se cumple que 
  # (∀x ∈ l) ∧ (∀y ∈ r) , x < y. 
  #
  # De forma que solo es necesario considerar las etiquetas, se tienen dos casos
  #
  # | (l
  # |   (ll (...) (...))
  # |   (lr (...) (...))) 
  # | (r 
  # |   (rl (...) (...))
  # |   (rr (...) (...))
  # 
  # * l.id > r.id en cuyo caso l debe ser el nodo raíz de r
  #
  #   | (l 
  #   |  (ll (...) (...))
  #   |  (merge (lr r))) 
  #
  # * l.id < r.id en cuyo caso r debe ser el nodo raíz de l 
  #
  #   | (r 
  #   |  (merge (l rl))
  #   |  (rr (...) (...))) 
  #
  #
  private def merge?(tl : Node(T) | Nil,tr : Node(T) | Nil)
    return (tl ? tl : tr) if !tl || !tr
    if tl.id > tr.id 
      tl.r = merge?(tl.r?,tr)
      return tl
    else 
      tr.l = merge?(tl,tr.l?)
      return tr
    end 
  end 


  # La función split se encarga de separar el treap en dos árboles 
  # 
  # l := {x ∈ T | x < val } 
  # r := {x ∈ T | x ≥ val } 
  # 
  # Para ello cada nodo se agrega a cada conjunto. 
  # 
  # | (n
  # |  (nl
  # |    (nll () ())
  # |    (nlr () ()))
  # |  (nr 
  # |    (nrl () ())
  # |    (nrr () ()))) 
  #
  # si el valor es menor o igual que el nodo actual, sabemos que el nodo 
  # y su lado derecho pertenecen al r. 
  #
  # | val <= n.val  
  # |
  # | r = (n
  # |       (? )
  # |       (nr (...)) 
  #
  # Resta determinar que elementos del lado izquierdo 'nl' pertenecen a 'r' y
  # cuales a 'l', para ello se llama recursivamente a la función. 
  # 
  # | l,rl = (split nl)
  #
  # de forma simétrica cuando el valor es mayor que el nodo actual sabemos 
  # que el nodo y si lado izquierdo pertenecen a 'l' y solo queda determinar 
  # que nodos del lado derecho pertenecen también a 'l'. 
  #
  # | n.val < val 
  # |
  # | l = (n
  # |       (nl (...))
  # |       (?) 
  #
  # | lr,r = (split nr)
  #
  #
  # En cada paso del split se actualiza el valor 'max_end'. 
  #
  private def split?(node : Node(T) | Nil,val : T,leq = true) 
    tr = tl = nil 
    return tl,tr if !node
  
    comp = (leq) ? (val <= node.val) : (val < node.val) 

    if (comp)
      tr = node
      l,r = split?(tr.l?,val,leq)
      tr.l = r
      tl = l
    else 
      tl = node 
      l,r = split?(tl.r?,val,leq) 
      tl.r = l
      tr = r
    end 
   
    return tl,tr
  end 


  # Para agregar un elemento se divide en dos el árbol en base al valor 
  #
  # l := {x | x < val} 
  # r := {x | x > val} 
  # m := val 
  #
  # después se une 'l' con un nuevo nodo 'm' y 'r'
  #
  # Si el valor ya se encuentra en el árbol solo se aumenta el contador 
  # que indica el número de veces que se encuentra repetido.
  #
  def push(val : T)
    n = Node.new(val)
    tl,tr = split?(@root,val)
    @root = merge?(merge?(tl,n),tr)
    self 
  end 


  # De manera análoga a la función push, se divide en tres partes el 
  # árbol,
  #
  # l := {x | x < val} 
  # m := {x | x == val} 
  # r := {x | x > val} 
  #
  # de forma que eliminar el valor solo consiste ignorar m al volver a unir
  # l y r. 
  #
  # Cuando un elemento esta repetido, se disminuye el número de veces que 
  # esta en el árbol, solo se elimina cuando la última copia es eliminada.
  #
  # /!\ Es importante notar que se libera la memoria del nodo, lo que 
  # invalida cualquier referencia que se tenga a el. 
  #
  # Note que dos intervalos a,b, el intervalo b es mayor  (a < b) si 
  # a.beg < b.beg o si a.beg == b.beg && a.end < b.end, de esta el 
  # sucesor de un intervalo 'r' es (r.beg,r.end+1), pues es el valor más
  # pequeño superior a 'r'.
  # 
  def pop?(val : T) 
    tl,m = split?(@root,val)
    m,tr = split?(m,val,false)
    @root = merge?(tl,tr)
    (m ? m.val : m)
  end 

  # Valor mínimo en el árbol 
  def min?(n = @root) (n && n.l?) ? self.min?(n.l?) : n end 

  # Valor máximo en el árbol 
  def max?(n = @root) (n && n.r?) ? self.max?(n.r?) : n end 

  # El predecesor es el máximo valor en el árbol que es inferior a 'val'. 
  # Para encontrarlo, empezando de la raíz, si 'val' > 'nodo' actual
  # significa que el nodo es candidato y el siguiente predecesor debe estar 
  # a la derecha del nodo, por otro lodo si es inferior el predecesor estará
  # a la izquierda.
  #
  def pred?(val : T, node = @root)
    pred = nil 
    while node 
      if val > node.val 
        pred = node 
        node = node.r?
      else
        node = node.l? 
      end 
    end 
    pred
  end 

  # El sucesor es el valor mínimo en el árbol que es inferior a 'val'. 
  # Para encontrarlo, empezando de la raíz, si 'val' < 'nodo' significa que 
  # el nodo es candidato y el siguiente sucesor debe estar a la izquierda del 
  # nodo, por otro lado si es superior el sucesor estará a la derecha de ese 
  # nodo.
  #
  def succ?(val : T,node = @root)
    succ = nil
    while node
      if val < node.val
        succ = node
        node = node.l?
      else 
        node = node.r? 
      end 
    end 
    succ
  end 

  # Encontrar el nodo con el valor especificado, si 'val' == 'nodo'
  private def find?(val,node = @root)
    if !node || val == node.val
      node 
    elsif val < node.val
      find?(val,node.l?) 
    else 
      find?(val,node.r?)
    end
  end

  # Determinar si existe un valor en el árbol 
  def includes?(val : T) !!find?(val) end 

  def inspect(io) io << "\n" << self end 
  def to_s(io) @root.try{|r| tree_str(r,io)} end 
  def tree_str(node = @root,io =STDOUT, depth = [] of String) 
    return io << "()" << "\n" if !node 

    io.puts "(#{node})"
    depth.push " " 
     
    depth.each{|c| io.print c} 
    io.print "├" 
    depth.push "│"
    tree_str node.l?,io,depth 
    depth.pop 

    depth.each{|c| io.print c} 
    io.print "└"
    depth.push " " 
    tree_str node.r?,io,depth 
    depth.pop

    depth.pop 
    return io 
  end 
end 



